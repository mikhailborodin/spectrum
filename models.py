import mapnik
import uuid
from spectrum import db
from sqlalchemy.types import TypeDecorator, CHAR
from sqlalchemy.dialects.postgresql import UUID
from geoalchemy2.types import Geometry


class GUID(TypeDecorator):
    """Platform-independent GUID type.

    Uses PostgreSQL's UUID type, otherwise uses
    CHAR(32), storing as stringified hex values.

    """
    impl = CHAR

    def load_dialect_impl(self, dialect):
        if dialect.name == 'postgresql':
            return dialect.type_descriptor(UUID())
        else:
            return dialect.type_descriptor(CHAR(32))

    def process_bind_param(self, value, dialect):
        if value is None:
            return value
        elif dialect.name == 'postgresql':
            return str(value)
        else:
            if not isinstance(value, uuid.UUID):
                return "%.32x" % uuid.UUID(value).int
            else:
                # hexstring
                return "%.32x" % value.int

    def process_result_value(self, value, dialect):
        if value is None:
            return value
        else:
            return uuid.UUID(value)


class Factor(db.Model):
    __tablename__ = 'factor'
    __table_args__ = {'extend_existing': True}
    id = db.Column(GUID(), primary_key=True)
    is_use_calc = db.Column(db.Boolean())
    factor_name = db.Column(db.Text())
    field_factor_name = db.Column(db.VARCHAR(length=100))
    source_table_name = db.Column(db.VARCHAR(length=50))
    geometry_type = db.Column(db.VARCHAR(length=10))
    quantity = db.Column(db.Integer())
    percent_influence = db.Column(db.Integer())
    type_condition = db.Column(db.VARCHAR(length=255))
    type_condition_standard = db.Column(db.VARCHAR(length=255))
    partition_step = db.Column(db.Integer())
    is_split = db.Column(db.Boolean())
    r1_condition = db.Column(db.Integer())
    r2_condition = db.Column(db.Integer())
    is_calc_potential = db.Column(db.Boolean())


class Project(db.Model):
    __tablename__ = 'project'
    __table_args__ = {'extend_existing': True}
    id = db.Column(GUID(), primary_key=True)
    table_name = db.Column(db.VARCHAR(length=255))
    name = db.Column(db.VARCHAR(length=255))
    comment = db.Column(db.VARCHAR(length=255))
    user_id = db.Column(GUID())


class ResultCalc(db.Model):
    __tablename__ = 'result_calc'
    id = db.Column(GUID(), primary_key=True)
    name = db.Column(db.VARCHAR(length=255))
    comment = db.Column(db.VARCHAR(length=255))
    project_id = db.Column(GUID(), db.ForeignKey('project.id'))


class SprKindLineObj(db.Model):
    __tablename__ = 'spr_kind_line_obj'
    id = db.Column(GUID(), primary_key=True)
    name = db.Column(db.VARCHAR(length=255))
    description = db.Column(db.VARCHAR(length=255))


class ObjContour(db.Model):
    __tablename__ = 'obj_contour'
    id = db.Column(GUID(), primary_key=True)
    project_id = db.Column(GUID(), db.ForeignKey('project.id'))
    geom_wgs84 = db.Column(Geometry(geometry_type='POLYGON', srid=4326))
    comment = db.Column(db.VARCHAR(length=255))


class ObjOrganization(db.Model):
    __tablename__ = 'obj_organization'
    id = db.Column(GUID(), primary_key=True)
    type1 = db.Column(db.VARCHAR(length=255))
    type2 = db.Column(db.VARCHAR(length=255))
    type3 = db.Column(db.VARCHAR(length=255))
    address = db.Column(db.VARCHAR(length=255))
    description = db.Column(db.VARCHAR(length=255))
    geom_wgs84 = db.Column(Geometry(geometry_type='POLYGON', srid=4326))


class ObjRegion(db.Model):
    __tablename__ = 'obj_region'
    id = db.Column(GUID(), primary_key=True)
    type = db.Column(db.VARCHAR(length=255))
    description = db.Column(db.VARCHAR(length=255))
    geom_wgs84 = db.Column(Geometry(geometry_type='POLYGON', srid=4326))


class ObjLine(db.Model):
    __tablename__ = 'obj_line'
    id = db.Column(GUID(), primary_key=True)
    type = db.Column(db.VARCHAR(length=255))
    description = db.Column(db.VARCHAR(length=255))
    geom_wgs84 = db.Column(Geometry(geometry_type='POLYGON', srid=4326))


class ObjPoint(db.Model):
    __tablename__ = 'obj_point'
    id = db.Column(GUID(), primary_key=True)
    type = db.Column(db.VARCHAR(length=255))
    description = db.Column(db.VARCHAR(length=255))
    geom_wgs84 = db.Column(Geometry(geometry_type='POLYGON', srid=4326))


class PotentialGeometry(db.Model):
    __tablename__ = 'potencial_geometry'
    id = db.Column(GUID(), primary_key=True)
    value = db.Column(db.INTEGER())
    color = db.Column(db.VARCHAR(length=7))
    cluster_num = db.Column(db.INTEGER())
    result_calc_id = db.Column(GUID(), db.ForeignKey('result_calc.id'))
    factor_id = db.Column(GUID(), db.ForeignKey('factor.id'))
    geom_wgs84 = db.Column(Geometry(geometry_type='POLYGON', srid=4326))


class Rosreestr(db.Model):
    __tablename__ = 'rosreestr_obj'
    id = db.Column(GUID(), primary_key=True)
    cadastral_num = db.Column(db.VARCHAR(length=50))
    geom_wgs84 = db.Column(Geometry(geometry_type='POLYGON', srid=4326))


class IntersectionObj(db.Model):
    __tablename__ = 'intersection_obj'
    id = db.Column(GUID(), primary_key=True)
    obj_contour_id = db.Column(db.VARCHAR(length=255))
    munic_obr_id = db.Column(db.VARCHAR(length=255))
    rosreestr_obj_id = db.Column(db.VARCHAR(length=255))
    geom_wgs84 = db.Column(Geometry(geometry_type='POLYGON', srid=4326))


class LineSymbolizer(db.Model):
    __tablename__ = 'linesymbolizer'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    stroke = db.Column(db.VARCHAR(length=10))
    stroke_width = db.Column(db.FLOAT())
    stroke_opacity = db.Column(db.FLOAT())
    stroke_linejoin = db.Column(db.VARCHAR(length=20))
    stroke_linecap = db.Column(db.VARCHAR(length=20))
    stroke_dasharray = db.Column(db.FLOAT())
    comp_op = db.Column(db.VARCHAR(length=20))
    smooth = db.Column(db.FLOAT())

    def to_dict(self):
        return {'stroke': self.stroke, 'stroke_width': self.stroke_width, 'stroke_opacity': self.stroke_opacity,
                'stroke_linejoin': self.stroke_linejoin, 'stroke_linecap': self.stroke_linecap,
                'stroke_dasharray': self.stroke_dasharray, 'comp_op': self.comp_op, 'smooth': self.smooth}

    def to_style(self):
        style = mapnik.LineSymbolizer()
        style.stroke = mapnik.Color(self.stroke)
        style.stroke_width = self.stroke_width
        style.opacity = self.stroke_opacity
        return style


class PolygonSymbolizer(db.Model):
    __tablename__ = 'polygonsymbolizer'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    fill = db.Column(db.VARCHAR(length=10))
    fill_opacity = db.Column(db.FLOAT())
    gamma = db.Column(db.FLOAT())
    comp_op = db.Column(db.VARCHAR(length=20))

    def to_dict(self):
        return {'fill': self.fill, 'fill_opacity': self.fill_opacity, 'gamma': self.gamma, 'comp_op': self.comp_op}

    def to_style(self):
        style = mapnik.PolygonSymbolizer()
        style.fill = mapnik.Color(self.fill)
        style.opacity = self.fill_opacity
        return style


class Style(db.Model):
    __tablename__ = 'style'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    layer = db.Column(db.VARCHAR(length=50))
    line_symbolizer = db.Column(db.Integer, db.ForeignKey('linesymbolizer.id'))
    polygon_symbolizer = db.Column(db.Integer, db.ForeignKey('polygonsymbolizer.id'))
    linesymbolizer = db.relationship('LineSymbolizer', backref=db.backref('Style', lazy='dynamic'))
    polygonsymbolizer = db.relationship('PolygonSymbolizer', backref=db.backref('Style', lazy='dynamic'))


MODELS = {
        'project': Project,
        'result_calc': ResultCalc,
        'obj_contour': ObjContour,
        'obj_contour_buffer': ObjContour,
        'obj_point': ObjPoint,
        'obj_line': ObjLine,
        'obj_region': ObjRegion,
        'obj_organization': ObjOrganization,
        'potential_geometry': PotentialGeometry,
        'factor': Factor,
        'rosreestr_obj': Rosreestr,
        'intersection_obj': IntersectionObj,
    }