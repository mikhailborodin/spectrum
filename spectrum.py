import json
import re
import mapnik
import hashlib
import os
import redis
from urllib import parse
from utils import compile_query, to_dict
from flask import Flask, Response, request, render_template
from flask_admin.contrib.sqla import ModelView
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from settings import DATABASE, BBOX_MAXX, BBOX_MAXY, BBOX_MINX, BBOX_MINY, TILE_WIDTH, TILE_HEIGHT, BASE_DIR, \
    CACHE_TIMEOUT, proj4
from utils import render_tile, deg2num
from werkzeug.contrib.fixers import ProxyFix

try:
    from GDAL.osgeo import gdal, ogr, osr
except ImportError:
    from osgeo import gdal, ogr, osr

from celery import Celery
from geoalchemy2 import Geometry
from geoalchemy2.functions import GenericFunction


r = redis.StrictRedis(host='localhost', port=6379, db=0)

class ST_SetSRID(GenericFunction):
    name = 'st_setsrid'
    type = Geometry


def make_celery(app):
    celery = Celery(app.import_name, backend=app.config['CELERY_RESULT_BACKEND'],
                    broker=app.config['CELERY_BROKER_URL'])
    celery.conf.update(app.config)
    TaskBase = celery.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)
    celery.Task = ContextTask
    return celery


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = '{0[engine]}{0[user]}:{0[password]}@{0[host]}:{0[port]}/{0[name]}'\
    .format(DATABASE)
SQLALCHEMY_TRACK_MODIFICATIONS = True
db = SQLAlchemy(app)

migrate = Migrate(app, db)

app.config.update(
    CELERY_BROKER_URL='redis://localhost:6379',
    CELERY_RESULT_BACKEND='redis://localhost:6379'
)
celery = make_celery(app)


def get_query(query, request, m):
    '''
    конструктор запроса
    :param query: исходный запрос, например, SELECT * FROM table
    :param request: объект request для передачи параметров запроса
    :param m:
    :return:
    SELECT ST_Intersection(obj_contour.geom_wgs84, rosreestr_obj_copy.geom_wgs84) as inter
    from obj_contour, rosreestr_obj_copy;
    '''
    if request.args.get('FILTER'):
        params = {i[0]: i[1] for i in map(lambda x: x.split('='), request.args.get('FILTER').split('&'))}
    else:
        params = request.args
    for attr, value in params.items():
        try:
            query = query.filter(getattr(m, attr) == value)
        except AttributeError:
            for fk in m.__table__.foreign_keys:
                if attr in list(map(lambda column: column.name, fk.column.table.columns)):
                    query = query.join(fk.column.table).filter(getattr(fk.column.table.c, attr) == value)
    return query


def get_source_table(model, request):
    '''
    функция для выборки данных из таблиц, указанных в модели Factor
    :param model: Factor
    :param request: объект request для передачи параметров запроса
    :return: название таблицы и условия поиска
    '''
    obj = get_query(db.session.query(model), request, model).all()[0]
    return obj.source_table_name, obj.type_condition


def get_source_request(model, condition):
    '''
    получение запроса из строки condition в модели Factor
    :param model: модель данных для выборки
    :param condition: исходная строка для фильтра
    :return: объект запроса
    '''
    class request:
        args = {}
    if model == 'obj_organization':
        value = re.findall(r'\*\d+\*', condition)[0]
        column = re.findall('ind\d+', condition)[0].replace('ind', 'type')
    else:
        value = re.findall(r'0x\d+', condition)[0]
        column = 'type'

    request.args[column] = value
    return request


@app.route('/api/<model>')
def query(model, only_sql=False):
    '''
    конструктор запроса в базу
    :param model: модель
    :param only_sql: если False то выполняется запрос и возвращается json
    :return: строка или json
    '''
    from models import MODELS
    if model == 'source_table':
        table, condition = get_source_table(MODELS['factor'], request)
        m = 'obj_%s' % table
        query = get_query(db.session.query(m), get_source_request(m, condition), MODELS[m])
    elif model == 'contoure_intersection':
        m1 = MODELS['obj_contour']
        m2 = MODELS['rosreestr_obj']
        query = db.session.query(m2.geom_wgs84).filter(ST_SetSRID(m2.geom_wgs84, 4326).ST_Intersects(ST_SetSRID(m1.geom_wgs84, 4326)))
    else:
        m = MODELS[model]
        query = get_query(db.session.query(m), request, m)
    if only_sql:
        if model == 'contoure_intersection':
            return re.sub(r'SELECT ST_AsEWKB(.+)',
                          'SELECT {}.geom_wgs84'.format(m2.__tablename__),
                          compile_query(query).decode('utf-8')
                          )
        else:
            return re.sub(r'SELECT .+',
                          'SELECT *',
                          compile_query(query).decode('utf-8')
                          )
    else:
        return Response(json.dumps([to_dict(obj) for obj in query.all()]), mimetype='application/json')


class Layer:
    def __init__(self, name, type_geom=None):
        from models import MODELS
        self.name = name
        self.type_geom = type_geom
        self.__g_field = 'geom_wgs84'
        self.__class = MODELS.get(name, None)

    def query_sql(self):
        return query(self.name, only_sql=True)

    @property
    def geometry_field(self):
        return self.__g_field

    @property
    def colors(self):
        if self.name == 'potential_geometry':
            return filter(lambda x: x[0] is not None, set(db.session.query(self.__class.color).all()))
        else:
            return []

    @geometry_field.setter
    def geometry_field(self):
        if self.name == 'obj_contour_buffer':
            self.__g_field = 'geom_buffer_wgs84'

    @property
    def symbolizer(self):
        from models import Style
        layer_symbolizer = db.session.query(Style).filter(Style.layer==self.name).all()
        if self.type_geom == 'polygon':
            return layer_symbolizer[0].polygon_symbolizer.to_style()
        elif self.type_geom == 'line':
            return layer_symbolizer[0].linesymbolizer.to_style()
        else:
            return layer_symbolizer[0].pointsymbolizer.to_style()


@app.route('/wms/')
def mapnik_render(params=None):
    '''
    функция генерирующая тайлы по схеме геосервера
    :return: tile image
    '''
    if not params:
        params = request.args
    m = mapnik.Map(int(params['width']), int(params['height']), proj4)
    type_geom, layer = params['layers'].split(':')
    image = render_tile(map=m, params=params, layers=Layer(layer, type_geom=type_geom), type_geom=type_geom)
    mapnik.render(m, image)
    return Response(image.tostring('png'), mimetype=params['format'])


@celery.task()
def generate_tile(layer, z, x, y, ext, get_params=None, save_as_file=False):
    stepx = (BBOX_MAXX - 1.0 - BBOX_MINX) / (2 ** int(z))
    stepy = (BBOX_MAXY - 1.0 - BBOX_MINY) / (2 ** int(z))
    box = [BBOX_MINX + int(x) * stepx,
           BBOX_MAXY - (int(y) + 1) * stepy,
           BBOX_MINX + (int(x) + 1) * stepx,
           BBOX_MAXY - int(y) * stepy]
    params = {
        'layers': layer,
        'srs': 'EPSG:3857',
        'bbox': ','.join(map(lambda x: str(x), box)),
        'width': TILE_WIDTH,
        'height': TILE_HEIGHT,
        'format': 'image/%s' % ext,
        'filter': get_params,
        'request': None
    }

    image_path = os.path.join('cache', layer, z, x)
    http_response = mapnik_render(params)
    dir = os.path.join(BASE_DIR, image_path)
    if not os.path.exists(dir):
        os.makedirs(dir)
    if request.args:
        with open(os.path.join(dir, '%s.%s?%s' % (y, ext, parse.urlencode(request.args))), 'wb') as t:
            t.write(http_response.data)
    else:
        with open(os.path.join(dir, '%s.%s' % (y, ext)), 'wb') as t:
            t.write(http_response.data)
    return http_response


@app.route('/tile/<layer>/<z>/<x>/<y>.<ext>')
def tile_render(layer, z, x, y, ext):
    '''
    контроллер для тайловых слоев
    &FILTER=factor_id=74689ec0-a1a6-46d4-a0e7-98940dfe8c77
    :return:
    '''
    path = os.path.join('cache', layer, z, x)
    tile_path = os.path.join(BASE_DIR, path)
    if request.args:
        tile_file = os.path.join(tile_path, '{}.{}?{}'.format(y, ext, parse.urlencode(request.args)))
    else:
        tile_file = os.path.join(tile_path, '{}.{}'.format(y, ext))
    tile_hash = hashlib.md5(tile_file.encode('utf-8'))
    try:
        cache_path = r.get(tile_hash.hexdigest())
        assert not (cache_path is None)
        with open(cache_path, 'rb') as tile:
            file_tile = tile.read()
            return Response(file_tile, mimetype='image/%s' % ext)
    except (FileNotFoundError, AssertionError):
        r.setex(tile_hash.hexdigest(), CACHE_TIMEOUT, tile_file)
        return generate_tile(layer, z, x, y, ext, request.args)


@app.route('/tile_generator/<layer>/')
def tile_generator(layer):
    '''
    запуск генератора тайлов
    :param layer:
    :return:
    '''
    filter = request.args.get('filter')
    x0, y0, x1, y1 = [55, 37, 56, 38]

    for z in range(10, 18):
        xtilemin, ytilemax = deg2num(x0, y0, z)
        xtilemax, ytilemin = deg2num(x1, y1, z)
        for x in range(xtilemin, xtilemax):
            for y in range(ytilemin, ytilemax):
                generate_tile.delay(layer, str(z), str(x), str(y), 'png', get_params=filter)
    return Response('started!')


@app.route('/map/')
def map_view():
    return render_template('map.html')


app.wsgi_app = ProxyFix(app.wsgi_app)
if __name__ == '__main__':
    from models import Style, LineSymbolizer, PolygonSymbolizer
    # from sqlalchemy.schema import CreateTable
    # print(CreateTable(PolygonSymbolizer.__table__))
    # print(CreateTable(LineSymbolizer.__table__))
    # print(CreateTable(Style.__table__))

    from flask_admin import Admin
    admin = Admin(app, name='spectrum', template_mode='bootstrap3')
    admin.add_view(ModelView(LineSymbolizer, db.session))
    admin.add_view(ModelView(PolygonSymbolizer, db.session))
    admin.add_view(ModelView(Style, db.session))
    app.secret_key = 'super secret key'
    app.config['SESSION_TYPE'] = 'filesystem'
    app.run(host='0.0.0.0')
