## Установка
### postgres
* sudo apt-get install postgresql-9.5
* create database <basename> owner <user>;
* create extension postgis;
### mapnik

#### you might have to update your outdated clang

sudo add-apt-repository -y ppa:ubuntu-toolchain-r/test

sudo apt-get update -y

sudo apt-get install -y gcc-6 g++-6 clang-3.8

export CXX="clang++-3.8" && export CC="clang-3.8"

#### install mapnik

git clone https://github.com/mapnik/mapnik mapnik-3.x --depth 10

cd mapnik-3.x

git submodule update --init

sudo apt-get install python zlib1g-dev clang make pkg-config curl

source bootstrap.sh

./configure CUSTOM_CXXFLAGS="-D_GLIBCXX_USE_CXX11_ABI=0" CXX=${CXX} CC=${CC}

make

make test

sudo make install

### Python
* create virtualenv
* source path/to/virtualenv/bin/activate
* pip install git+https://github.com/mapnik/python-mapnik.git
* pip install -r requirements.txt
* pip install gunicorn


#### Использование

основной url wms-сервиса – /wms/

GET параметры:

* SERVICE=WMS (не используется)
* VERSION=1.1.1 (не используется)
* REQUEST=GetMap (не используется)
* FORMAT=image%2Fpng формат изображения на вывод
* TRANSPARENT=true (не используется)
* STYLES (не используется)
* LAYERS=line:contoure_intersection (см. ниже)
* SRS=EPSG%3A4326 исходная СК, потом конвертируется в 4326
* WIDTH=768 ширина изображения
* HEIGHT=768 высота изображения
* BBOX=43.87%2C55.84%2C44.94%2C55.85 экстент тайла
* FILTER={key}={value}

##### LAYERS
* [polygon|line]:contoure_intersection отображения слоя пересечения линиями или полигонами (задача Отобразить на карте пересечения с объектом (табл obj_contoure) и объекты из табл rosreestr_obj) (/wms/?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=true&STYLES&LAYERS=line:contoure_intersection&SRS=EPSG%3A4326&WIDTH=768&HEIGHT=678&BBOX=43.87%2C55.84%2C44.94%2C55.85)
* [point|region|organiztion|line]:source_table задача для потенциала фактора отобразить исходные данные из табл obj_region/line/point/organization. /wms/?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=true&STYLES&LAYERS=line%3Asource_table&SRS=EPSG%3A4326&WIDTH=768&HEIGHT=678&BBOX=37.37%2C55.572%2C37.84%2C55.91&FILTER=id=74689ec0-a1a6-46d4-a0e7-98940dfe8c77
* spectrum:potential_geometry задача отобразить потенциал (табл potencial_geometry) фактора(табл Factor). Применяется фильтр factor_id параметром FILTER (/wms/?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=true&STYLES&LAYERS=spectrum%3Apotential_geometry&SRS=EPSG%3A4326&WIDTH=768&HEIGHT=678&BBOX=37.37%2C55.572%2C37.84%2C55.91&FILTER=factor_id=74689ec0-a1a6-46d4-a0e7-98940dfe8c77) и задача отобразить результаты расчета по проекту из табл resul_calc
* polygon:intersection_obj задача отобразить на карте пересечения (/wms/?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=true&STYLES&LAYERS=polygon:intersection_obj&SRS=EPSG%3A4326&WIDTH=768&HEIGHT=678&BBOX=43.87%2C55.84%2C44.94%2C55.85)