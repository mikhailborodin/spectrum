import math
import mapnik
from pyproj import Proj, transform
from settings import DATABASE
from sqlalchemy.ext.declarative import DeclarativeMeta
from sqlalchemy.sql import compiler


def to_dict(obj):
    '''
    функция генерации json из объекта sqlalchemy
    :param obj: object
    :return: json
    '''
    res = {column.key: (lambda o: o if isinstance(o, int) else str(o))(getattr(obj, attr))
           for attr, column in obj.__mapper__.c.items()}
    for attr, relation in obj.__mapper__.relationships.items():
        value = getattr(obj, attr)
        if value is None:
            res[relation.key] = None
        elif isinstance(value.__class__, DeclarativeMeta):
            res[relation.key] = value.to_dict(backref=obj.__table__)
        else:
            res[relation.key] = [i.to_dict(backref=obj.__table__)
                                 for i in value]
    return res


def compile_query(query):
    '''
    конвертирование запроса в строку
    :param query:
    :return:
    '''
    dialect = query.session.bind.dialect
    statement = query.statement
    comp = compiler.SQLCompiler(dialect, statement)
    comp.compile()
    enc = dialect.encoding
    params = {}
    for k, v in comp.params.items():
        if isinstance(v, str):
            params[k.encode(enc)] = b'\'%s\'' % v.encode(enc)
        elif isinstance(v, int):
            params[k.encode(enc)] = b'%d' % v
        else:
            params[k.encode(enc)] = v.encode(enc)
    return comp.string.encode(enc) % params


def render_tile(map=None, params=None, layers=None, type_geom='polygon', geom_field='geom_wgs84'):
    '''
    функция рендера тайла. Поддерживается только PostGIS источник данных
    :param map: объект карты
    :param params: передаваемые параметры SRS BBOX etc
    :param layers: название слоя
    :param type_geom: тип геометрии для стилизации
    :return: изображение
    '''
    m_layer = mapnik.Layer(layers.name, map.srs)
    m_layer.datasource = mapnik.PostGIS(
        host=DATABASE['host'],
        port=DATABASE['port'],
        user=DATABASE['user'],
        password=DATABASE['password'],
        dbname=DATABASE['name'],
        table='(%s) as q' % layers.query_sql(),
        geometry_field=geom_field,
        srid=4326
    )
    style = mapnik.Style()
    rule = mapnik.Rule()
    if layers.colors:
        for color in layers.colors:
            rule_on = mapnik.Rule()
            rule_on.filter = mapnik.Expression('[color] = "{}"'.format(color[0]))
            sym = mapnik.PolygonSymbolizer()
            sym.fill = mapnik.Color('%s' % color[0])
            rule_on.symbols.append(sym)
            style.rules.append(rule_on)
    else:
        sym = layers.symbolizer
        rule.symbols.append(sym)
        style.rules.append(rule)
    m_layer.styles.append('My style')
    map.append_style('My style', style)
    map.layers.append(m_layer)
    bbox = params['bbox'].split(',')
    p_dest = Proj(init="%s" % params['srs'] if 'srs' in params else params['crs'])
    p_source = Proj(init="EPSG:4326")
    minx_dest, miny_dest = transform(p_dest, p_source, bbox[0], bbox[1])
    maxx_dest, maxy_dest = transform(p_dest, p_source, bbox[2], bbox[3])
    extent = mapnik.Box2d(minx=float(minx_dest), miny=float(miny_dest), maxx=float(maxx_dest), maxy=float(maxy_dest))
    map.aspect_fix_mode = mapnik.aspect_fix_mode.RESPECT
    map.zoom_to_box(extent)
    image = mapnik.Image(map.width, map.height)
    return image


def deg2num(lat_deg, lon_deg, zoom):
    lat_rad = math.radians(lat_deg)
    n = 2.0 ** zoom
    xtile = int((lon_deg + 180.0) / 360.0 * n)
    ytile = int((1.0 - math.log(math.tan(lat_rad) + (1 / math.cos(lat_rad))) / math.pi) / 2.0 * n)
    return (xtile, ytile)
