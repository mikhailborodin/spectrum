import os
DATABASE = {
    'engine': 'postgresql://',
    'user': 'postgres',
    'password': 'postgres',
    'host': 'localhost',
    'port': '5433',
    'name': 'spectrum_1'
}
BBOX_MINX = -20037508.0
BBOX_MINY = -20037508.0
BBOX_MAXX = 20037508.0
BBOX_MAXY = 20037508.0
TILE_WIDTH = 256
TILE_HEIGHT = 256
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
CACHE_TIMEOUT = 60
proj4='+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs'
